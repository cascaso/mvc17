<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        table, th, td{
            border: 1px solid black;
        }
    </style>
    <title>Lista de libros</title>
</head>
<body>
    <header>Cabecera<hr></header>

    <content>
        <h1>Lista de libros</h1>

        <table>
            <tr>
                <th>id</th>
                <th>Título</th>
                <th>Autor</th>
                <th>Páginas</th>
                <th>Acciones</th>
            </tr>
            <?php foreach ($books as $book): ?>
            <tr>
                <td><?php echo $book->id ?></td>
                <td><?php echo $book->title ?></td>
                <td><?php echo $book->author ?></td>
                <td><?php echo $book->pages ?></td>
                <td>Editar - Borrar</td>
            </tr>
            <?php endforeach ?>
        </table>

        <a href="create">nuevo</a>

    </content>

    <footer><hr>Pie de página</footer>
</body>
</html>
